pragma solidity ^0.4.25;

/**
 * @title Operated
 * @dev The Operated contract has a list of ops addresses, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Operated {
    mapping(address => bool) private _ops;

    event OperatorChanged(
        address indexed operator,
        bool active
    );

    /**
     * @dev The Operated constructor sets the original ops account of the contract to the sender
     * account.
     */
    constructor() internal {
        _ops[msg.sender] = true;
        emit OperatorChanged(msg.sender, true);
    }

    /**
     * @dev Throws if called by any account other than the operations accounts.
     */
    modifier onlyOps() {
        require(isOps(), "only operations accounts are allowed to call this function");
        _;
    }

    /**
     * @return true if `msg.sender` is an operator.
     */
    function isOps() public view returns(bool) {
        return _ops[msg.sender];
    }

    /**
     * @dev Allows the current operations accounts to give control of the contract to new accounts.
     * @param _account The address of the new account
     * @param _active Set active (true) or inactive (false)
     */
    function setOps(address _account, bool _active) public onlyOps {
        _ops[_account] = _active;
        emit OperatorChanged(_account, _active);
    }
}
