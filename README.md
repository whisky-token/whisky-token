# Whisky Token

This repository hosts the official solidity contracts for the Whisky Token project.

# Copyright & License

Copyright (C) 2018 WHYTOKEN GmbH - All Rights Reserved

We do not offer any license for this code, therefore you are not allowed to use, distribute or modify it.
